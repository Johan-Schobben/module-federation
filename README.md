# Module federation

**This application requires angular cli version 12 to work!**

This repository is to show how you can use this applicaiton. However this is not part of the demonstration. There are multiple projects inside the project folder these are:

*   Shell
*   login
*   inventory
*   exam
*   cool-ui
*   auth

These projects mean the following

## shell

The shell application is the host application in which the other features are running. The shell will also provide some general styling and some shared state for all the other applications

## Cool ui

A ui libary used by the shell and the features

## Auth

A service that provides some global state for every user

## Login, inventory and exam-app

Features that will be laoded inside the application they use the components from cool-ui and get some global state from auth

## test the project

To start the project do the following

1.  Build every project using the build scripts in the package.json (`npm run build`)
2.  Serve every project using the serve scripts in the package.json

_If you now open the shell (running on port [5000](http://localhost:5000)) you see the applicaiton running you can check the devtools and see if you navigate to the exam app page that it will be fetched from the exam app server_

1.  close the exam app
2.  make some changes
3.  build the exam app agian
4.  serve the exam app again

Your changes will be applied and are visible from withing the shell. So people can start using the the new feature inside the shell without rebuilding the shell.

## Some usefull links

[webpack](https://webpack.js.org/concepts/module-federation/)  
[Module federation in angular](https://www.angulararchitects.io/en/aktuelles/the-microfrontend-revolution-part-2-module-federation-with-angular/)

Johan  
[Johan@cloudwise.nl](mailto:Johan@cloudwise.nl)