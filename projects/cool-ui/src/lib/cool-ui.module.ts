import { NgModule } from '@angular/core';
import { CoolUiComponent } from './cool-ui.component';



@NgModule({
  declarations: [
    CoolUiComponent,
  ],
  imports: [
  ],
  exports: [
    CoolUiComponent
  ]
})
export class CoolUiModule { }
