import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PinkButtonComponent } from './pink-button/pink-button.component';



@NgModule({
  declarations: [
    PinkButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PinkButtonComponent
  ]
})
export class ButtonModule { }
