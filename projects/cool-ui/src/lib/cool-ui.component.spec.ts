import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoolUiComponent } from './cool-ui.component';

describe('CoolUiComponent', () => {
  let component: CoolUiComponent;
  let fixture: ComponentFixture<CoolUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoolUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoolUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
