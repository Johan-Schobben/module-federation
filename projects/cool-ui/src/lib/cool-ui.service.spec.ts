import { TestBed } from '@angular/core/testing';

import { CoolUiService } from './cool-ui.service';

describe('CoolUiService', () => {
  let service: CoolUiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoolUiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
