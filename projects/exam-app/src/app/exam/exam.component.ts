import { Component, Inject, OnInit, Optional } from '@angular/core';
import { AuthService } from 'auth';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {
  user$?: Observable<string>;

  constructor(private authService: AuthService, @Optional() @Inject('host') host: string) { 
    console.log('running in', host)
  }
  

  ngOnInit(): void {
    this.user$ = this.authService.getUser();
  }
}
