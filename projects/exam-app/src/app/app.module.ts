import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ExamModule } from './exam/exam.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: "", component: HomeComponent},
      {path: "exams", loadChildren: () => import('./exam/exam.module').then(m => m.ExamModule)}
    ])
  ],
  providers: [
    {provide: 'host', useValue: 'exam-app' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
