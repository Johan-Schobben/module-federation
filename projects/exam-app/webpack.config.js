const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  ['auth', 'cool-ui']); /* only needed because the libs are local and not installed via NPM */


module.exports = {
  output: {
    uniqueName: "examApp",
    publicPath: "auto"
  },
  optimization: {
    runtimeChunk: false
  },   
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  plugins: [
    new ModuleFederationPlugin({
        name: "examApp",
        filename: "remoteEntry.js",
        exposes: {
            'ExamModule': './projects/exam-app/src/app/exam/exam.module.ts',
        },
        shared: {
          "@angular/core": { singleton: true, strictVersion: true }, 
          "@angular/common": { singleton: true, strictVersion: true }, 
          "@angular/common/http": { singleton: true, strictVersion: true }, 
          "@angular/router": { singleton: true, strictVersion: true },
          "rxjs": { singleton: true, strictVersion: true },
          // "cool-ui": { singleton: true, strictVersion: true }, Needed if cool-ui is installed via NPM

          ...sharedMappings.getDescriptors()
        }
        
    }),
    sharedMappings.getPlugin()
  ],
};
