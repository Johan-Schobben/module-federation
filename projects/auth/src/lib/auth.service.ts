import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: BehaviorSubject<string> = new BehaviorSubject<string>("");

  constructor() { }

  public setUser(user: string) {
    console.log(user)
    this.user.next(user)
  }

  public getUser(): Observable<string> {
    return this.user.asObservable();
  }

}
