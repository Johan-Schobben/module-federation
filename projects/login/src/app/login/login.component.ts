import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(event: Event) {
    const name = (event.target as HTMLInputElement).value;
    console.log(name);
    
    this.authService.setUser(name);
    this.router.navigateByUrl('/exam');
  }

}
