import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule, HeaderModule } from 'cool-ui';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HeaderModule,
    ButtonModule
  ],
  providers: [
    {provide: 'host', useValue: 'Shell'}
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
