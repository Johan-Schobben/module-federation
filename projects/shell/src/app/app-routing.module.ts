import { loadRemoteModule } from '@angular-architects/module-federation-runtime';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {component: HomeComponent, path: "" },
  { 
    path: "exam", 
    loadChildren: () => loadRemoteModule({
      remoteEntry: "http://localhost:3001/remoteEntry.js",
      exposedModule: "ExamModule",
      remoteName: "examApp"
    }).then(m => m.ExamModule)
  },
  { 
    path: "login", 
    loadChildren: () => loadRemoteModule({
      remoteEntry: "http://localhost:3005/remoteEntry.js",
      exposedModule: "LoginModule",
      remoteName: "login"
    }).then(m => m.LoginModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
