const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  ['auth', 'cool-ui']); /* only needed because the libs are local and not installed via NPM */

module.exports = {
  output: {
    uniqueName: "shell",
    publicPath: "auto"
  },
  optimization: {
    runtimeChunk: false
  },   
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  plugins: [
    new ModuleFederationPlugin({        
        remotes: {
            "modfed": "modfed@http://localhost:4200/remoteEntry.js",
            "examApp": "examApp@http://localhost:3001/remoteEntry.js",
            "inventory": "inventory@http://localhost:3002/remoteEntry.js",
            "login": "login@http://localhost:3005/remoteEntry.js"
        },

        shared: {
          "@angular/core": { singleton: true, strictVersion: true }, 
          "@angular/common": { singleton: true, strictVersion: true }, 
          "@angular/common/http": { singleton: true, strictVersion: true }, 
          "@angular/router": { singleton: true, strictVersion: true },
          "rxjs": { singleton: true, strictVersion: true },
          // "cool-ui": { singleton: true, eager: true },
          // "auth": {singleton: true, eager: true},
          ...sharedMappings.getDescriptors()
        }
        
    }),
    sharedMappings.getPlugin()
  ],
};
